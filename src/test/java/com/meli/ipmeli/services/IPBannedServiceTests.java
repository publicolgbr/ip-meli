package com.meli.ipmeli.services;

import com.meli.ipmeli.entities.IPBanned;
import com.meli.ipmeli.repositories.IPBannedRepository;
import com.meli.ipmeli.service.IPBannedService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@SpringBootTest
public class IPBannedServiceTests {

    @Autowired
    private IPBannedService ipBannedService;
    @MockBean
    private IPBannedRepository ipBannedRepository;

    @Test
    public void givenIPBannedWhenSaveIPBannedThenReturnIPBanned() {
        IPBanned ipBanned = new IPBanned(1L, "8.8.8.8");
        Mockito.when(this.ipBannedRepository.save(Mockito.any(IPBanned.class))).thenReturn(ipBanned);
        IPBanned ipBannedExpected = this.ipBannedService.save(ipBanned);
        assertThat(ipBanned, equalTo(ipBannedExpected));
    }

    @Test
    public void givenIPWhenSendIPThenReturnIPBanned(){
        IPBanned ipBanned = new IPBanned(1L, "8.8.8.8");
        Mockito.when(this.ipBannedRepository.findByIp(Mockito.anyString())).thenReturn(ipBanned);
        IPBanned ipBannedExpected = this.ipBannedService.getById("8.8.8.8");
        assertThat(ipBanned, equalTo(ipBannedExpected));
    }
}
