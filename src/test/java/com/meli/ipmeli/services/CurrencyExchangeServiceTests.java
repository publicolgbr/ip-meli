package com.meli.ipmeli.services;

import com.meli.ipmeli.entities.CurrencyExchange;
import com.meli.ipmeli.repositories.CurrencyExchangeRepository;
import com.meli.ipmeli.service.CurrencyExchangeService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@SpringBootTest
public class CurrencyExchangeServiceTests {
    @Autowired
    private CurrencyExchangeService currencyExchangeService;

    @MockBean
    private CurrencyExchangeRepository currencyExchangeRepository;

    @Test
    public void givenCurrencyExchangeWhenSaveCurrencyExchangeThenSave() {
        CurrencyExchange currencyExchange = new CurrencyExchange("cop", 4384.376072);
        Mockito.when(this.currencyExchangeRepository.save(Mockito.any())).thenReturn(currencyExchange);

        CurrencyExchange currencyExchangeExpected = currencyExchangeService.saveCurrencyExchange("cop", 4384.376072);

        assertThat(currencyExchange, equalTo(currencyExchangeExpected));
    }

    @Test
    public void givenRatesWhenSaveCurrencyExchangeThenSaveRates() {
        Map<String, Object> mapRates = new HashMap<>();
        mapRates.put("COP", 4384.376072);
        mapRates.put("CUP", 31.892936);

        List<CurrencyExchange> currencyExchanges = new ArrayList<>();
        CurrencyExchange currencyExchangeCOP = new CurrencyExchange("COP", 4384.376072);
        CurrencyExchange currencyExchangeCUP = new CurrencyExchange("CUP", 31.892936);
        currencyExchanges.add(currencyExchangeCOP);
        currencyExchanges.add(currencyExchangeCUP);

        Mockito.when(this.currencyExchangeRepository.saveAll(Mockito.any())).thenReturn(currencyExchanges);

        currencyExchangeService.saveCurrencyExchange(mapRates);

        Mockito.verify(this.currencyExchangeRepository, Mockito.times(1)).saveAll(currencyExchanges);
    }

    @Test
    public void givenCodeISOWhenGetCurrencyExchangeThenCurrencyExchange() {
        Optional<CurrencyExchange> currencyExchangeOptional = Optional.of(new CurrencyExchange("COP", 4384.376072));
        Mockito.when(this.currencyExchangeRepository.findById(Mockito.anyString())).thenReturn(currencyExchangeOptional);
        CurrencyExchange currencyExchangeExpected = this.currencyExchangeService.getCurrencyExchange("COP");
        assertThat(currencyExchangeOptional.get(),equalTo(currencyExchangeExpected));
    }

    @Test
    public void givenClearWhenClearCurrencyExchangeThenCurrencyExchange() {
        Mockito.doNothing().when(this.currencyExchangeRepository).deleteAll();
        this.currencyExchangeService.clearCurrencyExchange();
        Mockito.verify(this.currencyExchangeRepository, Mockito.times(1)).deleteAll();
    }
}
