package com.meli.ipmeli.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.meli.ipmeli.entities.CountryCurrency;
import com.meli.ipmeli.entities.CurrencyExchange;
import com.meli.ipmeli.entities.IPCountry;
import com.meli.ipmeli.entities.IPInfo;
import com.meli.ipmeli.exceptions.ServiceException;
import com.meli.ipmeli.service.CountryCurrencyService;
import com.meli.ipmeli.service.CurrencyExchangeService;
import com.meli.ipmeli.service.IPBannedService;
import com.meli.ipmeli.service.IPInfoService;
import com.meli.ipmeli.utils.IPToCountry;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@SpringBootTest
public class IPInfoServiceTests {
    @Autowired
    private IPInfoService ipInfoService;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private IPToCountry ipToCountry;
    @MockBean
    private IPBannedService ipBannedService;
    @MockBean
    private CountryCurrencyService countryCurrencyService;
    @MockBean
    private CurrencyExchangeService currencyExchangeService;

    @Test
    public void givenIPWhenSendIPThenReturnIPInfo() throws ExecutionException, InterruptedException, JsonProcessingException, ServiceException {
        IPCountry ipCountry = new IPCountry("CO", "COL", "Colombia", "\uD83C\uDDE8\uD83C\uDDF4");
        String jsonResponseCountryInfo = "{\"name\":\"Colombia\",\"topLevelDomain\":[\".co\"],\"alpha2Code\":\"CO\",\"alpha3Code\":\"COL\",\"callingCodes\":[\"57\"],\"capital\":\"Bogotá\",\"altSpellings\":[\"CO\",\"Republic of Colombia\",\"República de Colombia\"],\"region\":\"Americas\",\"subregion\":\"South America\",\"population\":48759958,\"latlng\":[4.0,-72.0],\"demonym\":\"Colombian\",\"area\":1141748.0,\"gini\":55.9,\"timezones\":[\"UTC-05:00\"],\"borders\":[\"BRA\",\"ECU\",\"PAN\",\"PER\",\"VEN\"],\"nativeName\":\"Colombia\",\"numericCode\":\"170\",\"currencies\":[{\"code\":\"COP\",\"name\":\"Colombian peso\",\"symbol\":\"$\"}],\"languages\":[{\"iso639_1\":\"es\",\"iso639_2\":\"spa\",\"name\":\"Spanish\",\"nativeName\":\"Español\"}],\"translations\":{\"de\":\"Kolumbien\",\"es\":\"Colombia\",\"fr\":\"Colombie\",\"ja\":\"コロンビア\",\"it\":\"Colombia\",\"br\":\"Colômbia\",\"pt\":\"Colômbia\",\"nl\":\"Colombia\",\"hr\":\"Kolumbija\",\"fa\":\"کلمبیا\"},\"flag\":\"https://restcountries.eu/data/col.svg\",\"regionalBlocs\":[{\"acronym\":\"PA\",\"name\":\"Pacific Alliance\",\"otherAcronyms\":[],\"otherNames\":[\"Alianza del Pacífico\"]},{\"acronym\":\"USAN\",\"name\":\"Union of South American Nations\",\"otherAcronyms\":[\"UNASUR\",\"UNASUL\",\"UZAN\"],\"otherNames\":[\"Unión de Naciones Suramericanas\",\"União de Nações Sul-Americanas\",\"Unie van Zuid-Amerikaanse Naties\",\"South American Union\"]}],\"cioc\":\"COL\"}";
        String jsonResponseCountryCurrency = "{\"success\":true,\"timestamp\":1619026444,\"base\":\"EUR\",\"date\":\"2021-04-21\",\"rates\":{\"AED\":4.418486,\"AFN\":93.28914,\"ALL\":123.121511,\"AMD\":628.216659,\"ANG\":2.159346,\"AOA\":792.278764,\"ARS\":111.907344,\"AUD\":1.551193,\"AWG\":2.165611,\"AZN\":2.041954,\"BAM\":1.959071,\"BBD\":2.428888,\"BDT\":102.006724,\"BGN\":1.955294,\"BHD\":0.453487,\"BIF\":2367.406301,\"BMD\":1.20295,\"BND\":1.599894,\"BOB\":8.306682,\"BRL\":6.699715,\"BSD\":1.202975,\"BTC\":2.1592004e-5,\"BTN\":90.736584,\"BWP\":13.011967,\"BYN\":3.124321,\"BYR\":23577.826978,\"BZD\":2.424782,\"CAD\":1.502461,\"CDF\":2402.292053,\"CHF\":1.103165,\"CLF\":0.030378,\"CLP\":838.338509,\"CNY\":7.808233,\"COP\":4354.764495,\"CRC\":738.883315,\"CUC\":1.20295,\"CUP\":31.878184,\"CVE\":110.913,\"CZK\":25.858256,\"DJF\":213.787951,\"DKK\":7.43556,\"DOP\":68.447486,\"DZD\":159.619083,\"EGP\":18.862865,\"ERN\":18.046554,\"ETB\":49.766255,\"EUR\":1,\"FJD\":2.466281,\"FKP\":0.873793,\"GBP\":0.863472,\"GEL\":4.156156,\"GGP\":0.873793,\"GHS\":6.953046,\"GIP\":0.873793,\"GMD\":61.410581,\"GNF\":11951.311684,\"GTQ\":9.282759,\"GYD\":251.673271,\"HKD\":9.339526,\"HNL\":28.988062,\"HRK\":7.57462,\"HTG\":100.14645,\"HUF\":362.12055,\"IDR\":17471.951709,\"ILS\":3.930418,\"IMP\":0.873793,\"INR\":90.682725,\"IQD\":1758.111945,\"IRR\":50650.224859,\"ISK\":150.99438,\"JEP\":0.873793,\"JMD\":181.651574,\"JOD\":0.852855,\"JPY\":130.044927,\"KES\":130.46009,\"KGS\":101.999962,\"KHR\":4875.557719,\"KMF\":492.965174,\"KPW\":1082.655552,\"KRW\":1344.42911,\"KWD\":0.362317,\"KYD\":1.002446,\"KZT\":519.754286,\"LAK\":11334.198108,\"LBP\":1838.108102,\"LKR\":230.972278,\"LRD\":207.545546,\"LSL\":17.129659,\"LTL\":3.551999,\"LVL\":0.727652,\"LYD\":5.395241,\"MAD\":10.73633,\"MDL\":21.647778,\"MGA\":4559.181281,\"MKD\":61.723796,\"MMK\":1696.202573,\"MNT\":3429.1933,\"MOP\":9.619797,\"MRO\":429.45307,\"MUR\":48.604358,\"MVR\":18.615677,\"MWK\":950.330474,\"MXN\":23.915568,\"MYR\":4.957326,\"MZN\":66.836043,\"NAD\":17.129953,\"NGN\":457.705925,\"NIO\":42.284012,\"NOK\":10.037971,\"NPR\":145.178414,\"NZD\":1.668606,\"OMR\":0.463095,\"PAB\":1.202975,\"PEN\":4.455703,\"PGK\":4.252397,\"PHP\":58.219788,\"PKR\":184.289191,\"PLN\":4.552842,\"PYG\":7757.738927,\"QAR\":4.379898,\"RON\":4.924759,\"RSD\":117.744816,\"RUB\":92.175467,\"RWF\":1184.304626,\"SAR\":4.511531,\"SBD\":9.623757,\"SCR\":16.805909,\"SDG\":458.924641,\"SEK\":10.12468,\"SGD\":1.598096,\"SHP\":0.873793,\"SLL\":12276.1088,\"SOS\":704.928791,\"SRD\":17.026591,\"STD\":24936.123975,\"SVC\":10.526285,\"SYP\":1512.791162,\"SZL\":17.129756,\"THB\":37.664383,\"TJS\":13.716409,\"TMT\":4.222356,\"TND\":3.303906,\"TOP\":2.728172,\"TRY\":9.854929,\"TTD\":8.163605,\"TWD\":33.774639,\"TZS\":2789.650283,\"UAH\":33.861489,\"UGX\":4345.08922,\"USD\":1.20295,\"UYU\":53.272478,\"UZS\":12657.443261,\"VEF\":257227142094.3881,\"VND\":27758.079466,\"VUV\":131.769814,\"WST\":3.045542,\"XAF\":656.940188,\"XAG\":0.045432,\"XAU\":0.000671,\"XCD\":3.251034,\"XDR\":0.838255,\"XOF\":656.204361,\"XPF\":119.754043,\"YER\":301.279128,\"ZAR\":17.132659,\"ZMK\":10827.967037,\"ZMW\":26.753732,\"ZWL\":387.350249}}";
        CountryCurrency countryCurrency = objectMapper.readValue(jsonResponseCountryCurrency, CountryCurrency.class);
        CurrencyExchange currencyExchange = new CurrencyExchange("COP",4354.764495);
        Mockito.when(this.ipBannedService.getById(Mockito.anyString())).thenReturn(null);

        Mockito.when(this.ipToCountry.getInfoIpLocation(Mockito.anyString())).thenReturn(CompletableFuture.completedFuture(ipCountry));
        Mockito.when(this.ipToCountry.getCountryInfo(Mockito.anyString())).thenReturn(CompletableFuture.completedFuture(jsonResponseCountryInfo));

        Mockito.when(this.countryCurrencyService.getCountryCurrency()).thenReturn(new CountryCurrency());
        Mockito.when(this.ipToCountry.getCountryCurrency()).thenReturn(CompletableFuture.completedFuture(countryCurrency));
        Mockito.when(this.countryCurrencyService.saveCountryCurrency(Mockito.any(CountryCurrency.class))).thenReturn(countryCurrency);
        Mockito.doNothing().when(this.currencyExchangeService).saveCurrencyExchange(Mockito.any());
        Mockito.when(this.currencyExchangeService.getCurrencyExchange(Mockito.anyString())).thenReturn(currencyExchange);

        IPInfo ipInfo = this.ipInfoService.getIPInfo("186.29.39.142");

        assertThat(ipInfo.getNameCountry(),equalTo("Colombia"));
        //assertThat(ipInfo.getCurrency(),equalTo("COP"));
        assertThat(ipInfo.getIsoCountry(),equalTo("CO"));
        assertThat(ipInfo.getExchangePrice(),equalTo("4354.764495"));
    }
}
