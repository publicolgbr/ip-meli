package com.meli.ipmeli.services;

import com.meli.ipmeli.entities.CountryCurrency;
import com.meli.ipmeli.repositories.CountryCurrencyRepository;
import com.meli.ipmeli.service.CountryCurrencyService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@SpringBootTest
public class CountryCurrencyServiceTests {

    @Autowired
    private CountryCurrencyService countryCurrencyService;

    @MockBean
    private CountryCurrencyRepository countryCurrencyRepository;

    @Test
    public void givenCountryCurrencyWhenSaveCountryCurrencyThenSave() {
        Map<String, Object> stringObjectMap = new HashMap<>();
        stringObjectMap.put("COP", 4384.376072);
        CountryCurrency countryCurrency = new CountryCurrency("1", true, 1618963744L, "EUR", "2021-04-21", stringObjectMap);
        Mockito.when(this.countryCurrencyRepository.save(Mockito.any())).thenReturn(countryCurrency);

        CountryCurrency countryCurrencyExpected = countryCurrencyService.saveCountryCurrency(countryCurrency);

        assertThat(countryCurrency, equalTo(countryCurrencyExpected));
    }

    @Test
    public void givenGetWhenGetCountryCurrencyThenCountryCurrency() {
        Map<String, Object> stringObjectMap = new HashMap<>();
        stringObjectMap.put("COP", 4384.376072);
        Optional<CountryCurrency> countryCurrencyOptional = Optional.of(new CountryCurrency("1", true, 1618963744L, "EUR", "2021-04-21", stringObjectMap));
        Mockito.when(this.countryCurrencyRepository.findById(Mockito.anyString())).thenReturn(countryCurrencyOptional);

        CountryCurrency countryCurrencyExpected = countryCurrencyService.getCountryCurrency();

        assertThat(countryCurrencyOptional.get(), equalTo(countryCurrencyExpected));
    }

    @Test
    public void givenClearWhenClearCountryCurrencyThenCountryCurrencyIsEmpty() {
        Mockito.doNothing().when(this.countryCurrencyRepository).deleteAll();
        this.countryCurrencyService.clearCountryCurrency();
        Mockito.verify(this.countryCurrencyRepository, Mockito.times(1)).deleteAll();
    }

}
