package com.meli.ipmeli.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meli.ipmeli.entities.IPCountry;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.ExecutionException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@SpringBootTest
public class IPToCountryTests {

    @Autowired
    private IPToCountry ipToCountry;

    @MockBean
    private RestTemplate restTemplate;

    @Test
    public void givenIPWhenCallApiCountryThenReturnIpCountry() throws ExecutionException, InterruptedException {
        IPCountry ipCountry = new IPCountry();
        ipCountry.setCountryCode("CO");
        ipCountry.setCountryCode3("COL");
        ipCountry.setCountryName("Colombia");
        ipCountry.setCountryEmoji("\uD83C\uDDE8\uD83C\uDDF4");
        Mockito
                .when(restTemplate.getForObject("https://api.ip2country.info/ip?186.29.39.142", IPCountry.class))
                .thenReturn(ipCountry);
        IPCountry ipCountryExpected = this.ipToCountry.getInfoIpLocation("186.29.39.142").get();

        assertThat(ipCountry, equalTo(ipCountryExpected));
    }
}
