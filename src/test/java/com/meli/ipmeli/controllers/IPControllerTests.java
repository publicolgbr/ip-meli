package com.meli.ipmeli.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.meli.ipmeli.entities.IPBanned;
import com.meli.ipmeli.entities.IPInfo;
import com.meli.ipmeli.service.IPBannedService;
import com.meli.ipmeli.service.IPInfoService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(IPController.class)
public class IPControllerTests {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private IPInfoService ipInfoService;
    @MockBean
    private IPBannedService ipBannedService;

    @Test
    public void givenIPWhenSendIPThenResponseStatusOk() throws Exception {
        IPInfo ipInfo = new IPInfo("Colombia", "co", "cop", "4532.63");
        Mockito.when(this.ipInfoService.getIPInfo(Mockito.anyString())).thenReturn(ipInfo);
        ResultActions resultActions = this.mockMvc.perform(get("/api/ip/1.2.3.4"));
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void givenIPWhenSendIPThenResponseContentNotNull() throws Exception {
        IPInfo ipInfo = new IPInfo("Colombia", "co", "cop", "4532.63");
        Mockito.when(this.ipInfoService.getIPInfo(Mockito.anyString())).thenReturn(ipInfo);
        ResultActions resultActions = this.mockMvc.perform(get("/api/ip/1.2.3.4"));
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nameCountry", is("Colombia")))
                .andExpect(jsonPath("$.isoCountry", is("co")))
                .andExpect(jsonPath("$.currency", is("cop")))
                .andExpect(jsonPath("$.exchangePrice", is("4532.63")));
    }

    @Test
    public void givenIPBannedWhenPostIPBannedThenResponseStatusOk() throws Exception {
        IPBanned ipBanned = new IPBanned(1L, "8.8.8.8");
        Mockito.when(this.ipBannedService.save(Mockito.any())).thenReturn(ipBanned);
        ResultActions resultActions = this.mockMvc
                .perform(post("/api/ip")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(ipBanned)));
        resultActions.andExpect(status().isOk());
    }

    @Test
    public void givenIPBannedWhenPostIPBannedThenNotNull() throws Exception {
        IPBanned ipBanned = new IPBanned(1L, "8.8.8.8");
        Mockito.when(this.ipBannedService.save(Mockito.any())).thenReturn(ipBanned);
        ResultActions resultActions = this.mockMvc
                .perform(post("/api/ip")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(ipBanned)));
        resultActions.andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.ip", is("8.8.8.8")));
    }
}
