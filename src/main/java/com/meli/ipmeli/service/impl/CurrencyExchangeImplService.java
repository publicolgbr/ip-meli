package com.meli.ipmeli.service.impl;

import com.meli.ipmeli.entities.CurrencyExchange;
import com.meli.ipmeli.repositories.CurrencyExchangeRepository;
import com.meli.ipmeli.service.CurrencyExchangeService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Log4j2
@Service
public class CurrencyExchangeImplService implements CurrencyExchangeService {

    private final CurrencyExchangeRepository currencyExchangeRepository;

    public CurrencyExchangeImplService(CurrencyExchangeRepository currencyExchangeRepository) {
        this.currencyExchangeRepository = currencyExchangeRepository;
    }

    @Override
    public CurrencyExchange saveCurrencyExchange(String codeIso, Double exchange) {
        return currencyExchangeRepository.save(new CurrencyExchange(codeIso, exchange));
    }

    @Override
    public void saveCurrencyExchange(Map<String, Object> ratesMap) {
        List<CurrencyExchange> currencyExchanges = new ArrayList<>();
        for (Map.Entry<String, Object> entry : ratesMap.entrySet()) {
            currencyExchanges.add(new CurrencyExchange(entry.getKey(), Double.parseDouble(entry.getValue().toString())));
        }
        currencyExchangeRepository.saveAll(currencyExchanges);
    }

    @Override
    public CurrencyExchange getCurrencyExchange(String codeIso) {
        return currencyExchangeRepository.findById(codeIso).orElse(new CurrencyExchange("NAN", 0.0));
    }

    @Override
    public void clearCurrencyExchange() {
        currencyExchangeRepository.deleteAll();
    }
}
