package com.meli.ipmeli.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.meli.ipmeli.entities.*;
import com.meli.ipmeli.exceptions.ServiceException;
import com.meli.ipmeli.service.CountryCurrencyService;
import com.meli.ipmeli.service.CurrencyExchangeService;
import com.meli.ipmeli.service.IPBannedService;
import com.meli.ipmeli.service.IPInfoService;
import com.meli.ipmeli.utils.IPToCountry;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Log4j2
@Service
public class IPInfoImplService implements IPInfoService {

    private final IPToCountry ipToCountry;

    private final IPBannedService ipBannedService;

    private final CountryCurrencyService countryCurrencyService;

    private final CurrencyExchangeService currencyExchangeService;

    public IPInfoImplService(IPToCountry ipToCountry, IPBannedService ipBannedService, CountryCurrencyService countryCurrencyService, CurrencyExchangeService currencyExchangeService) {
        this.ipToCountry = ipToCountry;
        this.ipBannedService = ipBannedService;
        this.countryCurrencyService = countryCurrencyService;
        this.currencyExchangeService = currencyExchangeService;
    }

    @Override
    public IPInfo getIPInfo(String ip) throws ServiceException {
        IPInfo ipInfo = new IPInfo();
        IPBanned ipBannedCheck = ipBannedService.getById(ip);
        if (ipBannedCheck != null)
            throw new ServiceException("Banned ip");
        try {
            IPCountry ipCountry = ipToCountry.getInfoIpLocation(ip).get();
            String countryCurrency = ipToCountry.getCountryInfo(ipCountry.getCountryCode()).get();
            CurrencyExchange currencyExchange = getCurrencyExchangePerTime(countryCurrency);
            ipInfo.setNameCountry(ipCountry.getCountryName());
            ipInfo.setIsoCountry(ipCountry.getCountryCode());
            ipInfo.setCurrency(countryCurrency);
            ipInfo.setExchangePrice(currencyExchange.getExchange().toString());
        } catch (JsonProcessingException | InterruptedException | ExecutionException e) {
            throw new ServiceException("Third API Error", e);
        }
        return ipInfo;
    }

    public CurrencyExchange getCurrencyExchangePerTime(String currencyISO) throws ExecutionException, InterruptedException {
        long timestampNow = new Date().getTime() / 1000;
        CurrencyExchange currencyExchangeOut;
        CountryCurrency countryCurrencyCheck = countryCurrencyService.getCountryCurrency();
        if (countryCurrencyCheck.getId() == null) {
            CountryCurrency countryCurrency = ipToCountry.getCountryCurrency().get();
            countryCurrency.setId("1");
            countryCurrencyService.saveCountryCurrency(countryCurrency);
            currencyExchangeService.saveCurrencyExchange(countryCurrency.getRates());
        } else {
            long timestampConcurrent = countryCurrencyCheck.getTimestamp();
            long diffTime = Math.abs(timestampNow - timestampConcurrent);
            if (TimeUnit.HOURS.convert(diffTime, TimeUnit.SECONDS) >= 1) {
                countryCurrencyService.clearCountryCurrency();
                currencyExchangeService.clearCurrencyExchange();
                CountryCurrency countryCurrency = ipToCountry.getCountryCurrency().get();
                countryCurrency.setId("1");
                countryCurrencyService.saveCountryCurrency(countryCurrency);
                currencyExchangeService.saveCurrencyExchange(countryCurrency.getRates());
            }
        }
        currencyExchangeOut = currencyExchangeService.getCurrencyExchange(currencyISO);
        return currencyExchangeOut;
    }
}
