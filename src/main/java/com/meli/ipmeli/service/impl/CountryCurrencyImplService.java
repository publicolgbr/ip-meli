package com.meli.ipmeli.service.impl;

import com.meli.ipmeli.entities.CountryCurrency;
import com.meli.ipmeli.repositories.CountryCurrencyRepository;
import com.meli.ipmeli.service.CountryCurrencyService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Log4j2
@Service
public class CountryCurrencyImplService implements CountryCurrencyService {

    private final CountryCurrencyRepository countryCurrencyRepository;

    public CountryCurrencyImplService(CountryCurrencyRepository countryCurrencyRepository) {
        this.countryCurrencyRepository = countryCurrencyRepository;
    }

    @Override
    public CountryCurrency saveCountryCurrency(CountryCurrency countryCurrency) {
        return countryCurrencyRepository.save(countryCurrency);
    }

    @Override
    public CountryCurrency getCountryCurrency() {
        Optional<CountryCurrency> optionalCountryCurrency = countryCurrencyRepository.findById("1");
        return optionalCountryCurrency.orElseGet(CountryCurrency::new);
    }

    @Override
    public void clearCountryCurrency() {
        countryCurrencyRepository.deleteAll();
    }
}
