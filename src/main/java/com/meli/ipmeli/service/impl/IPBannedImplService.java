package com.meli.ipmeli.service.impl;

import com.meli.ipmeli.entities.IPBanned;
import com.meli.ipmeli.repositories.IPBannedRepository;
import com.meli.ipmeli.service.IPBannedService;
import org.springframework.stereotype.Service;

@Service
public class IPBannedImplService implements IPBannedService {

    private final IPBannedRepository ipBannedRepository;

    public IPBannedImplService(IPBannedRepository ipBannedRepository) {
        this.ipBannedRepository = ipBannedRepository;
    }

    @Override
    public IPBanned save(IPBanned ipBanned) throws IllegalArgumentException  {
        return ipBannedRepository.save(ipBanned);
    }

    @Override
    public IPBanned getById(String ip) throws IllegalArgumentException {
        return ipBannedRepository.findByIp(ip);
    }
}
