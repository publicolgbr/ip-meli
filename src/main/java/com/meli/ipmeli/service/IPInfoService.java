package com.meli.ipmeli.service;

import com.meli.ipmeli.entities.IPInfo;
import com.meli.ipmeli.exceptions.ServiceException;

public interface IPInfoService {
    IPInfo getIPInfo(String ip) throws ServiceException;
}
