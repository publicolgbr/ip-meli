package com.meli.ipmeli.service;

import com.meli.ipmeli.entities.CountryCurrency;

public interface CountryCurrencyService {
    CountryCurrency saveCountryCurrency(CountryCurrency countryCurrency);
    CountryCurrency getCountryCurrency();
    void clearCountryCurrency();
}