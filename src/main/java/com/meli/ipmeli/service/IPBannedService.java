package com.meli.ipmeli.service;

import com.meli.ipmeli.entities.IPBanned;

public interface IPBannedService {
    IPBanned save(IPBanned ipBanned) throws IllegalArgumentException;

    IPBanned getById(String ip) throws IllegalArgumentException;
}
