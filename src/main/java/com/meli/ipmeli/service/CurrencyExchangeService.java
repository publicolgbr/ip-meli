package com.meli.ipmeli.service;

import com.meli.ipmeli.entities.CurrencyExchange;

import java.util.Map;

public interface CurrencyExchangeService {
    CurrencyExchange saveCurrencyExchange(String codeIso, Double exchange);
    void saveCurrencyExchange(Map<String,Object> ratesMap);
    CurrencyExchange getCurrencyExchange(String codeIso);
    void clearCurrencyExchange();
}
