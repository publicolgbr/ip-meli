package com.meli.ipmeli.repositories;

import com.meli.ipmeli.entities.CurrencyExchange;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CurrencyExchangeRepository extends CrudRepository<CurrencyExchange,String> {
}
