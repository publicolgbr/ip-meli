package com.meli.ipmeli.repositories;

import com.meli.ipmeli.entities.IPBanned;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPBannedRepository extends JpaRepository<IPBanned, Long> {
    IPBanned findByIp(String ip);
}
