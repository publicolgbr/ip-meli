package com.meli.ipmeli.repositories;

import com.meli.ipmeli.entities.CountryCurrency;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryCurrencyRepository extends CrudRepository<CountryCurrency, String> {
}
