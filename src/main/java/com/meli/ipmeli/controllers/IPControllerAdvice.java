package com.meli.ipmeli.controllers;

import com.meli.ipmeli.exceptions.ServiceException;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice(assignableTypes = IPController.class)
@RequestMapping
@Log4j2
public class IPControllerAdvice {
    @ExceptionHandler({ServiceException.class, IllegalArgumentException.class, Exception.class})
    public ResponseEntity<IPResponseVo> handleServiceException(final Throwable exception) {
        log.error("Internal server error", exception);
        IPResponseVo response = new IPResponseVo(HttpStatus.BAD_REQUEST.toString(), exception.getMessage());
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

}
