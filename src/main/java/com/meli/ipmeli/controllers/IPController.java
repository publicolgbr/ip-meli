package com.meli.ipmeli.controllers;

import com.meli.ipmeli.entities.IPBanned;
import com.meli.ipmeli.entities.IPInfo;
import com.meli.ipmeli.exceptions.ServiceException;
import com.meli.ipmeli.service.IPBannedService;
import com.meli.ipmeli.service.IPInfoService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/ip")
public class IPController {

    private final IPInfoService ipInfoService;

    private final IPBannedService ipBannedService;

    public IPController(IPInfoService ipInfoService, IPBannedService ipBannedService) {
        this.ipInfoService = ipInfoService;
        this.ipBannedService = ipBannedService;
    }

    @GetMapping("/{ip}")
    public IPInfo getIPInfo(@PathVariable("ip") String ip) throws ServiceException {
        return ipInfoService.getIPInfo(ip);
    }

    @PostMapping()
    public IPBanned setIPBanned(@RequestBody IPBanned ipBanned) throws IllegalArgumentException{
        return ipBannedService.save(ipBanned);
    }
}
