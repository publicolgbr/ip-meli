package com.meli.ipmeli.controllers;

import lombok.Getter;

public class IPResponseVo {
    @Getter
    private String status;

    @Getter
    private String message;

    public IPResponseVo(String status, String message) {
        this.status = status;
        this.message = message;
    }
}
