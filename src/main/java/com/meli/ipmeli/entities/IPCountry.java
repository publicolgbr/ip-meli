package com.meli.ipmeli.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IPCountry implements Serializable {
    private String countryCode;
    private String countryCode3;
    private String countryName;
    private String countryEmoji;
}
