package com.meli.ipmeli.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IPInfo {
    private String nameCountry;
    private String isoCountry;
    private String currency;
    private String exchangePrice;
}
