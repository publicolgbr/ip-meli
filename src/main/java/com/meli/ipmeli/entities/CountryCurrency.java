package com.meli.ipmeli.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.redis.core.RedisHash;

import javax.persistence.Id;
import java.io.Serializable;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RedisHash("CountryCurrency")
public class CountryCurrency implements Serializable {
    @Id
    private String id;
    private Boolean success;
    private Long timestamp;
    private String base;
    private String date;
    private Map<String, Object> rates;
}
