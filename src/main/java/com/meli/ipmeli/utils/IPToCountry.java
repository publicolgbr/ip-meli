package com.meli.ipmeli.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.meli.ipmeli.config.JsonConfig;
import com.meli.ipmeli.entities.CountryCurrency;
import com.meli.ipmeli.entities.IPCountry;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicReference;

@Log4j2
@Service
public class IPToCountry {

    @Value("${meli.third.api.geolocation.url}")
    private String ipToCountryUrl;

    @Value("${meli.third.api.country.url}")
    private String countryUrl;

    @Value("${meli.third.api.currency.url}")
    private String currencyUrl;

    @Value("${meli.third.api.currency.key}")
    private String currencyKey;

    private final RestTemplate restTemplate;

    private final ObjectMapper objectMapper;

    public IPToCountry(RestTemplate restTemplate, ObjectMapper objectMapper) {
        this.restTemplate = restTemplate;
        this.objectMapper = objectMapper;
    }

    @Async
    public CompletableFuture<IPCountry> getInfoIpLocation(String ip) {
        IPCountry ipCountry = restTemplate.getForObject(ipToCountryUrl + ip, IPCountry.class);
        return CompletableFuture.completedFuture(ipCountry);
    }

    @Async
    public CompletableFuture<String> getCountryInfo(String codeISO) throws JsonProcessingException {
        URI targetUrl = UriComponentsBuilder.fromUriString(countryUrl)
                .path(codeISO)
                .build()
                .encode()
                .toUri();
        String response = restTemplate.getForObject(targetUrl, String.class);
        return CompletableFuture.completedFuture(getCodeCurrency(response).get());
    }

    @Async
    public CompletableFuture<CountryCurrency> getCountryCurrency() {
        URI targetUrl = UriComponentsBuilder.fromUriString(currencyUrl)
                .queryParam("access_key", currencyKey)
                .build()
                .encode()
                .toUri();
        CountryCurrency countryCurrency = restTemplate.getForObject(targetUrl, CountryCurrency.class);
        log.info(countryCurrency.getBase());
        return CompletableFuture.completedFuture(countryCurrency);
    }

    public AtomicReference<String> getCodeCurrency(String response) throws JsonProcessingException {
        AtomicReference<String> codeCurrency = new AtomicReference<>("");
        ObjectNode node = objectMapper.readValue(response, ObjectNode.class);
        if (node.has("currencies")) {
            node.get("currencies").spliterator().forEachRemaining((f) -> codeCurrency.set(f.findValue("code").asText()));
        }
        return codeCurrency;
    }
}
