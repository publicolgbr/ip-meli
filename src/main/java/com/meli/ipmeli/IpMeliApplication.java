package com.meli.ipmeli;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IpMeliApplication {

    public static void main(String[] args) {
        SpringApplication.run(IpMeliApplication.class, args);
    }

}
