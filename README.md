# Getting Started

### Reference Documentation

For further reference, please consider the following sections:

* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.5/gradle-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.4.5/gradle-plugin/reference/html/#build-image)
* [Testcontainers Postgres Module Reference Guide](https://www.testcontainers.org/modules/databases/postgres/)
* [Testcontainers](https://www.testcontainers.org/)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.4.5/reference/htmlsingle/#boot-features-developing-web-applications)
* [Flyway Migration](https://docs.spring.io/spring-boot/docs/2.4.5/reference/htmlsingle/#howto-execute-flyway-database-migrations-on-startup)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.4.5/reference/htmlsingle/#boot-features-jpa-and-spring-data)

### Guides

The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)

### Additional Links

These additional references should also help you:

* [Gradle Build Scans – insights for your project's build](https://scans.gradle.com#gradle)

## How to run
execute on the project root
```console
docker-compose up
```
execute silent mode
```console
docker-compose up -d
```

## Endpoints

*base url* http://localhost:8080

**IP info**
----
    obtain ip information
* **URL**
  
  /api/ip/{ip}

* **Method:**

  `GET`

*  **URL Params**

   **Required:**

  `ip=[String]`

* **Success Response:**

    * **Code:** 200 <br />
      **Content:** `{ "nameCountry": "Colombia", "isoCountry": "CO", "currency": "COP", "exchangePrice": "4349.317335 }`

* **Error Response:**

    * **Code:** 400 BAD REQUEST <br />
      **Content:** `{ status: "400", message: "No Such File" }`

* **Sample Call:**
    ```console
    https://api.ip2country.info/ip?5.6.7.8
    ```

**IP Banned**
----
    banned ip
* **URL**
  
  /api/ip

* **Method:**

  `POST`

* **Data Params (payload)**

  **Required:**

  `{ip: "8.8.8.8"}`

* **Success Response:**

    * **Code:** 200 <br />
      **Content:** `{ id: 1, ip: "8.8.8.8" }`

* **Error Response:**

    * **Code:** 400 BAD REQUEST <br />
      **Content:** `{ status: "400", message: "Malformed" }`